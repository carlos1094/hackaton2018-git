package com.example.eddy.hackaton2k18mapa;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CrearBrigadaActivity extends AppCompatActivity{

    private static final String CERO = "0";
    private static final String DOS_PUNTOS = ":";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la hora hora
    final int hora = c.get(Calendar.HOUR_OF_DAY);
    final int minuto = c.get(Calendar.MINUTE);


    //Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);


    //Widgets
    EditText etHora;
    EditText et_Fecha;
    EditText etHoraFin;
    EditText et_FechaFin;
    ImageButton ibObtenerHora;
    ImageButton ibObtenerFecha;
    ImageButton ibObtenerHoraFin;
    ImageButton ibObtenerFechaFin;
    Button crearBrigada;
    LatLng coordenada;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_brigada);
        etHora = (EditText) findViewById(R.id.et_mostrar_hora_picker);
        etHoraFin = (EditText) findViewById(R.id.et_mostrar_horaFin_picker);
        et_Fecha = (EditText) findViewById(R.id.et_mostrar_fecha_picker);
        et_FechaFin = (EditText) findViewById(R.id.et_mostrar_fechaFin_picker);
        //Widget ImageButton del cual usaremos el evento clic para obtener la hora
        ibObtenerHora = (ImageButton) findViewById(R.id.ib_obtener_hora);
        ibObtenerFecha = (ImageButton) findViewById(R.id.ib_obtener_fecha);
        ibObtenerHoraFin = (ImageButton) findViewById(R.id.ib_obtener_horaFin);
        ibObtenerFechaFin= (ImageButton) findViewById(R.id.ib_obtener_fechaFin);

        //Evento setOnClickListener - clic
        Bundle bundle = getIntent().getParcelableExtra("Coordenada");
        coordenada = bundle.getParcelable("coor");
        crearBrigada = findViewById(R.id.button2);
        crearBrigada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String horaInicio = String.valueOf(etHora.getText());
                String fechaInicio = String.valueOf(et_Fecha.getText());
                String horaFin = String.valueOf(etHoraFin.getText());
                String fechaFin = String.valueOf(et_FechaFin.getText());
                String hI [] = horaInicio.split(" ");
                String hF [] = horaFin.split(" ");
                String fI [] = fechaInicio.split("/");
                String fF [] = fechaFin.split("/");
                String hSI[] = hI[0].split(":");
                String hSF[] = hF[0].split(":");

                Brigada brigada = new Brigada("LiderRojo", Integer.parseInt(fI[2]),Integer.parseInt(fF[2]),
                        Integer.parseInt(fI[1]),Integer.parseInt(fF[1]),Integer.parseInt(fI[0]),Integer.parseInt(fF[0]),
                        Integer.parseInt(hSI[0]),Integer.parseInt(hSF[0]), Integer.parseInt(hSI[1]),
                        Integer.parseInt(hSF[1]), coordenada);
                MapsActivity.brigadasDisponibles.add(brigada);
                MapsActivity.misBrigadas.add(brigada);
                MapsActivity.inscripcionBrigadas.add(brigada);
                Context context = getApplicationContext();
                CharSequence text = "Agregada a Mis Brigadas";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                Calendar beginTime = Calendar.getInstance();
                beginTime.set(Integer.parseInt(fI[2]), Integer.parseInt(fI[1]), Integer.parseInt(fI[0]),
                        Integer.parseInt(hSI[0]), Integer.parseInt(hSI[1]));
                Calendar endTime = Calendar.getInstance();
                endTime.set(Integer.parseInt(fF[2]), Integer.parseInt(fF[1]), Integer.parseInt(fF[0]),
                        Integer.parseInt(hSF[0]), Integer.parseInt(hSF[1]));
                Intent calIntent = new Intent(Intent.ACTION_INSERT);
                calIntent.setType("vnd.android.cursor.item/event");
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis());
                calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis());
                calIntent.putExtra(CalendarContract.Events.TITLE, "Mi Brigada");
                calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, coordenada.toString());
                calIntent.putExtra(CalendarContract.Events.DESCRIPTION, "Super Brigada de limpieza");
                calIntent.putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
                calIntent.putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
                startActivity(calIntent);

                Intent intent = new Intent();
                setResult(8888, intent);
                finish();
            }
        });
    }

    public void onClick(View v){
// DialogFragment datePicker = new DatePickerFragment();
// datePicker.show(getSupportFragmentManager(), "date picker");
        switch (v.getId()){
            case R.id.ib_obtener_fecha:
                obtenerFecha(et_Fecha);
                break;
            case R.id.ib_obtener_hora:
                obtenerHora(etHora);
                break;
            case R.id.ib_obtener_fechaFin:
                obtenerFecha(et_FechaFin);
                break;
            case R.id.ib_obtener_horaFin:
                obtenerHora(etHoraFin);
                break;
        }
    }

    private void obtenerFecha(final EditText editText){
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10)? CERO + String.valueOf(mesActual):String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                editText.setText(diaFormateado + "/" + mesFormateado + "/" + year);


            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        },anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();

    }

    private void obtenerHora(final EditText editText){
        TimePickerDialog recogerHora = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                //Formateo el hora obtenido: antepone el 0 si son menores de 10
                String horaFormateada =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                //Formateo el minuto obtenido: antepone el 0 si son menores de 10
                String minutoFormateado = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                //Obtengo el valor a.m. o p.m., dependiendo de la selección del usuario
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "a.m.";
                } else {
                    AM_PM = "p.m.";
                }
                //Muestro la hora con el formato deseado
                editText.setText(horaFormateada + ":" + minutoFormateado + " " + AM_PM);
            }
            //Estos valores deben ir en ese orden
            //Al colocar en false se muestra en formato 12 horas y true en formato 24 horas
            //Pero el sistema devuelve la hora en formato 24 horas
        }, hora, minuto, false);

        recogerHora.show();
    }
    /*
    TextView txtView1 , txtView2;
    DatePickerDialog.OnDateSetListener onDateSetListener;
    int anio, mesInicio, diaInicio;
    int anioFin, mesFin, diaFin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_brigada);

        txtView1 = (TextView) findViewById(R.id.textInicio);
        txtView2 = (TextView) findViewById(R.id.textFin);


    }

    public void onClick(View v){
// DialogFragment datePicker = new DatePickerFragment();
// datePicker.show(getSupportFragmentManager(), "date picker");
        switch (v.getId()){
            case R.id.textInicio:
                showDatePickerDialog(txtView1);
                break;
            case R.id.textFin:
                showDatePickerDialog(txtView2);
                break;
        }
    }

    private void showDatePickerDialog(final TextView editText){
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, month);
                c.set(Calendar.DAY_OF_MONTH, day);
                if(editText.getId() == R.id.textInicio){
                    anio = year;
                    mesInicio = month;
                    diaInicio = day;
                } else if (editText.getId() == R.id.textFin){
                    anioFin = year;
                    mesFin = month;
                    diaFin = day;
                }
                String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
                editText.setText(currentDateString);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void irActivity(){

    }
*/

// @Override
// public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
// Calendar c = Calendar.getInstance();
// c.set(Calendar.YEAR, year);
// c.set(Calendar.MONTH, month);
// c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
// String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
// txtView1.setText(currentDateString);
// }
}