package com.example.eddy.hackaton2k18mapa;

/**
 * Created by Eddy on 3/16/2018.
 */

public class Reto {

    private int pesoOrganico;
    private int pesoInorganico;
    private int pesoSanitario;
    private int tiempo;
    private int participantes;

    public Reto() {
    }

    public Reto(int pesoOrganico, int pesoInorganico, int pesoSanitario, int tiempo, int participantes) {
        this.pesoOrganico = pesoOrganico;
        this.pesoInorganico = pesoInorganico;
        this.pesoSanitario = pesoSanitario;
        this.tiempo = tiempo;
        this.participantes = participantes;
    }

    public int getPesoOrganico() {
        return pesoOrganico;
    }

    public void setPesoOrganico(int pesoOrganico) {
        this.pesoOrganico = pesoOrganico;
    }

    public int getPesoInorganico() {
        return pesoInorganico;
    }

    public void setPesoInorganico(int pesoInorganico) {
        this.pesoInorganico = pesoInorganico;
    }

    public int getPesoSanitario() {
        return pesoSanitario;
    }

    public void setPesoSanitario(int pesoSanitario) {
        this.pesoSanitario = pesoSanitario;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getParticipantes() {
        return participantes;
    }

    public void setParticipantes(int participantes) {
        this.participantes = participantes;
    }
}
