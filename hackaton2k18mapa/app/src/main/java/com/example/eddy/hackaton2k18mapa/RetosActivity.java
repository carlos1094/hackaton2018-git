package com.example.eddy.hackaton2k18mapa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class RetosActivity extends AppCompatActivity {


    private SeekBar sb1, sb2, sb3, sb4, sb5;

    private TextView tx1, tx2, tx3, tx4, tx5;

    int pesoOrganico, pesoInorganico, pesoSanitario, duracion, visitantes;

    private Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retos);
        tx1 = (TextView) findViewById(R.id.textView7);

        tx2 = (TextView) findViewById(R.id.textView8);

        tx3 = (TextView) findViewById(R.id.textView9);

        tx4 = (TextView) findViewById(R.id.textView10);

        tx5 = (TextView) findViewById(R.id.textView11);

        sb1 = (SeekBar) findViewById(R.id.seekBar);

        sb2 = (SeekBar) findViewById(R.id.seekBar2);

        sb3 = (SeekBar) findViewById(R.id.seekBar3);

        sb4 = (SeekBar) findViewById(R.id.seekBar4);

        sb5 = (SeekBar) findViewById(R.id.seekBar5);

        sb1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tx1.setText(String.valueOf(i) + " kg");
                pesoOrganico = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sb2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tx2.setText(String.valueOf(i) + " kg");
                pesoInorganico = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        sb3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tx3.setText(String.valueOf(i) + " kg");
                pesoSanitario = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sb4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tx4.setText(String.valueOf(i) + " hrs");
                duracion = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sb5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tx5.setText(String.valueOf(i));
                visitantes = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}



