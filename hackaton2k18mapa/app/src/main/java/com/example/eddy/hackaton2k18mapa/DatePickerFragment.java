package com.example.eddy.hackaton2k18mapa;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by Eddy on 3/16/2018.
 */

public class DatePickerFragment extends DialogFragment {
    // @NonNull
// @Override
// public Dialog onCreateDialog(Bundle savedInstanceState) {
// Calendar cal = Calendar.getInstance();
// int year = cal.get(Calendar.YEAR);
// int month = cal.get(Calendar.MONTH);
// int day = cal.get(Calendar.DAY_OF_MONTH);
//
// return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(),
// year, month, day);
// }
    private DatePickerDialog.OnDateSetListener listener;

    public static DatePickerFragment newInstance(DatePickerDialog.OnDateSetListener listener) {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setListener(listener);
        return fragment;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    @NonNull
    public DatePickerDialog onCreateDialog(Bundle savedInstanceState) {
// Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

// Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }
}