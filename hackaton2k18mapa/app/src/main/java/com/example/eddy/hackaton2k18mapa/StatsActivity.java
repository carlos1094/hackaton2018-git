package com.example.eddy.hackaton2k18mapa;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public class StatsActivity extends AppCompatActivity {
    public static ArrayList<Brigada> brigadasInvitado = new ArrayList<>();
    public static ArrayList<Brigada> brigadasCreadas= new ArrayList<>();
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/


//Dos eventos creados, por participacion
       /* Brigada brigadaInvitada = new Brigada();
        Brigada brigadaInvitada2 = new Brigada();

        brigadasInvitado.add(brigadaInvitada);
        brigadasInvitado.add(brigadaInvitada2);

        brigadaInvitada.setNombreLider("Rojo");

        brigadaInvitada.setDiaInicio(13);
        brigadaInvitada.setHoraInicio(12);

        brigadaInvitada2.setNombreLider("Juancho");
        brigadaInvitada2.setDiaInicio(16);
        brigadaInvitada2.setHoraInicio(8);
//
// brigada responsable
        Brigada brigadaResponsable = new Brigada();
        Brigada brigadaResponsable2 = new Brigada();

        brigadasCreadas.add(brigadaResponsable);
        brigadasCreadas.add(brigadaResponsable2);

        brigadaResponsable.setNombreLider("Rojito");
        brigadaResponsable.setDiaInicio(13);
        brigadaResponsable.setHoraInicio(12);*/

        listView=(ListView)findViewById(R.id.list_view1);
        listView.setBackgroundColor(Color.GRAY);
        brigadasCreadas = cambiarFormatoArreglo(MapsActivity.misBrigadas);
        brigadasInvitado=cambiarFormatoArreglo(MapsActivity.inscripcionBrigadas);

//Brigadas Participando
        final ArrayAdapter<Brigada> adapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1, brigadasCreadas);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(),RetosActivity.class);
                startActivity(intent);
            }
        });
        listView.setAdapter(adapter);

        listView=(ListView)findViewById(R.id.list_view);
        listView.setBackgroundColor(Color.GRAY);

//Brigadas Participando
        final ArrayAdapter<Brigada> adapter1 = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1, brigadasInvitado);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
        listView.setAdapter(adapter1);

    }

    public ArrayList cambiarFormatoArreglo(ArrayList<Brigada> brigada){
        ArrayList nuevo = new ArrayList<>();

        for(int i=0;i<brigada.size();i++){
            nuevo.add("Nombre lider:"+brigada.get(i).getNombreLider()+" Dia de inicio:"+brigada.get(i).getDiaInicio()+" Hora de inicio:"+brigada.get(i).getHoraInicio());
        }
        return nuevo;
    }
}
