package com.example.eddy.hackaton2k18mapa;

/**
 * Created by Eddy on 3/16/2018.
 */


import com.google.android.gms.maps.model.LatLng;

public class Brigada {
    private Reto reto;
    private String nombreLider;
    private int anioInicio;
    private int anioFin;
    private int mesInicio;
    private int mesFin;
    private int diaInicio;
    private int diaFin;
    private int horaInicio;
    private int horaFin;
    private int minInicio;
    private int minFin;
    private LatLng coordenadas;

    public Brigada(){

    }


    public Brigada(LatLng coordenadas) {
        this.coordenadas = coordenadas;
    }

    public Brigada(String nombreLider, int anioInicio, int anioFin, int mesInicio, int mesFin,
                   int diaInicio, int diaFin, int horaInicio, int horaFin, int minInicio, int minFin,
                   LatLng coordenadas) {
        this.nombreLider = nombreLider;
        this.anioInicio = anioInicio;
        this.anioFin = anioFin;
        this.mesInicio = mesInicio;
        this.mesFin = mesFin;
        this.diaInicio = diaInicio;
        this.diaFin = diaFin;
        this.horaInicio = horaInicio;
        this.horaFin = horaFin;
        this.minInicio = minInicio;
        this.minFin = minFin;
        this.coordenadas = coordenadas;
    }

    public int getMinInicio() {
        return minInicio;
    }

    public void setMinInicio(int minInicio) {
        this.minInicio = minInicio;
    }

    public int getMinFin() {
        return minFin;
    }

    public void setMinFin(int minFin) {
        this.minFin = minFin;
    }

    public Reto getReto() {
        return reto;
    }

    public void setReto(Reto reto) {
        this.reto = reto;
    }

    public int getMesFin() {
        return mesFin;
    }

    public void setMesFin(int mesFin) {
        this.mesFin = mesFin;
    }

    public LatLng getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(LatLng coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getNombreLider() {
        return nombreLider;
    }

    public void setNombreLider(String nombreLider) {
        this.nombreLider = nombreLider;
    }

    public int getAnioInicio() {
        return anioInicio;
    }

    public void setAnioInicio(int anioInicio) {
        this.anioInicio = anioInicio;
    }

    public int getAnioFin() {
        return anioFin;
    }

    public void setAnioFin(int anioFin) {
        this.anioFin = anioFin;
    }

    public int getMesInicio() {
        return mesInicio;
    }

    public void setMesInicio(int mesInicio) {
        this.mesInicio = mesInicio;
    }

    public int getDiaInicio() {
        return diaInicio;
    }

    public void setDiaInicio(int diaInicio) {
        this.diaInicio = diaInicio;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public int getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(int horaFin) {
        this.horaFin = horaFin;
    }

    public int getDiaFin() {
        return diaFin;
    }

    public void setDiaFin(int diaFin) {
        this.diaFin = diaFin;
    }

    @Override
    public String toString() {
        return "Brigada{" +
                "nombreLider='" + nombreLider + '\'' +
                ", anioInicio=" + anioInicio +
                ", anioFin=" + anioFin +
                ", mesInicio=" + mesInicio +
                ", diaInicio=" + diaInicio +
                ", horaInicio=" + horaInicio +
                ", horaFin=" + horaFin +
                ", diaFin=" + diaFin +
                '}';
    }

    @Override
    public boolean equals(Object o){
        return o instanceof Brigada && ((Brigada)o).getCoordenadas().equals(this.getCoordenadas());
    }
}